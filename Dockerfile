FROM tomcat:9-jdk21-openjdk

# Siguiente hace accesible la ruta http:localhost:8080/webapp
# ADD src/main /usr/local/tomcat/webapps 

# Siguiente hace accesible la ruta  http:localhost:8080/tp_integrador_java
# ADD target/tp_integrador_java.war /usr/local/tomcat/webapps

# Siguiente hace accesible la ruta http:localhost:8080/
ADD target/tp_integrador_java.war /usr/local/tomcat/webapps/ROOT.war

# opciones para attach debug en vscode
ENV JAVA_TOOL_OPTIONS="-agentlib:jdwp=transport=dt_socket,address=0.0.0.0:8000,server=y,suspend=n"

EXPOSE 8080 8000

CMD ["catalina.sh", "run"]
