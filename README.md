# Trabajo integrador Java

> **Codo a Codo - 23558** - Federico Schroh

## Tecnologías utilizadas
| Frontend (Server Side Rendered) | Backend               |
| :------------------------------ | :-------------------- |
| HTML, CSS y Javascript          | Java Servlets y MySql |

# Link video
Video demostrativo de funcionamiento haciendo click [Aquí](https://https://www.youtube.com/watch?v=9HqZK3vr41M)

# Instalación
> **Importante:** La documentación presentada requiere tener ejecutando **Docker** en el equipo para poder 
> correr tanto la base de datos como el servidor web. Los comandos de instalación están basados en **bash de linux**.
> 
> Si prefiere no usar Docker habrá que configurar **Mysql 8.0.26** y **Apache Tomcat 9** en el equipo de destino y realizar 
> la instalación manual.

## Ejecutando el instalador
Se debe ejecutar el archivo **install.sh** que se encuentra dentro del folder **scripts**. Para ello deberá pararse sobre la ruta
scripts antes de ejecutar la instalación
```bash
cd ./scripts
```
Luego se debe ejecutar el instalador
```bash
./install.sh
```
Esta acción creará la base de datos, las tablas y poblará datos maestros si fuera necesario, también compilará e iniciará
la aplicación web del proyecto. 
Puede inspeccionar y modificar valores por defecto si desea. Si opta por modificar los parámetros por defecto, debe 
asegurarse de configurar la variable de entorno MYSQL_CONN_STRING con el string de conexión completo para conectar 
a la base de datos o la aplicación no podrá funcionar. Esta variable también deberá ser configurada si opta por realizar 
una instalación manual sin docker.

Ejemplo básico de parámetros requeridos:
```bash
MYSQL_CONN_STRING="jdbc:mysql://<IP_O_NOMBRE_SERVIDOR_BD>:<PUERTO_BD>/<NOMBRE_BD>?user=<USER_DB>&password=<PASSWORD_DB>"
```
El instalador hará que se borren los datos si se vuelve a ejecutar, ya que borra y crean nuevas imágenes y contenedores 
cada vez que se ejecute. Es útil para instalación inicial o cuando se quiere aplicar cambios en código y levantar una 
nueva versión de prueba.

Si sólo quiere regenerar la app durante desarrollo, se puede correr el script **install_app.sh** de forma independiente.

## Verificar instalación
Para poder verificar puede revisar que ambos contenedores se encuentran en ejecución
```bash
docker ps
```

también puede revisar logs de ser necesario
```bash
docker logs tp-java-app
docker logs tp-java-database
```

si todo salió bien, entonces debería poder abrir el navegador web y navegar hacia la aplicación sin problemas
```bash
http://localhost:8080
```
En este momento ya debería ver el sitio en el navegador.

# Detener o iniciar aplicación
Si ya está corriendo y solo quiere apagar o encender el servidor web y de base de datos, puede hacerlo con comandos de docker
de forma directa.

Detener servicios
```bash
docker stop tp-java-app
docker stop tp-java-database
```

Iniciar servicios
```bash
docker start tp-java-app
docker start tp-java-database
```