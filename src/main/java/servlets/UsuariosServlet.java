package servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.Session;

@WebServlet("/usuarios")
public class UsuariosServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (!Session.isLogged(req)) {
            res.sendRedirect("login");
            return;
        }

        RequestDispatcher dispatcher = req.getRequestDispatcher("/views/UsuariosAdmin.jsp");
        dispatcher.forward(req, res);
    }
}
