package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import models.OradorModel;
import repositories.OradoresRepository;

@WebServlet("/solicitud-orador")
public class SolicitudOradorServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String queryString = req.getQueryString();
        HttpSession session = req.getSession();

        if (queryString.contains("aprobados=true")) {
            OradoresRepository repo = new OradoresRepository();
            ArrayList<OradorModel> oradores = repo.getOradoresAprobadosSummary();
            session.setAttribute("oradoresAprobados", oradores);
            res.sendRedirect("index#oradores-aprobados");
        } else if (queryString.contains("aprobados=false")) {
            session.removeAttribute("oradoresAprobados");
            res.sendRedirect("index#oradores-aprobados");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        OradorModel orador = new OradorModel();
        orador.email = req.getParameter("email");
        orador.nombres = req.getParameter("nombres");
        orador.apellidos = req.getParameter("apellidos");
        orador.tema = req.getParameter("tema");
        HttpSession session = req.getSession();

        try {
            OradoresRepository repo = new OradoresRepository();
            repo.saveOradorFromIndex(orador);
            session.setAttribute("solicitudOradorSuccess", "Su solicitud fue ingresada exitosamente");
        } catch (Exception e) {
            session.setAttribute("solicitudOradorError", "Su solicitud no pudo ser ingresada");
        }

        res.sendRedirect("/index#contacto");
    }
}
