package servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.Session;

@WebServlet("/dashboard")
public class DashboardServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (!Session.isLogged(req)) {
            res.sendRedirect("login");
            return;
        }

        if (Session.isAdmin(req)) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/views/AdminDashboard.jsp");
            dispatcher.forward(req, res);
        }

        RequestDispatcher dispatcher = req.getRequestDispatcher("/views/UsersDashboard.jsp");
        dispatcher.forward(req, res);
    }
}
