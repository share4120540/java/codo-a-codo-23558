package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.OradorModel;
import repositories.OradoresRepository;
import utils.Session;

@WebServlet("/oradores")
public class OradoresServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (!Session.isLogged(req)) {
            res.sendRedirect("login");
            return;
        }

        OradoresRepository repo = new OradoresRepository();
        ArrayList<OradorModel> oradores = repo.getOradores();

        HttpSession session = req.getSession();
        session.setAttribute("oradores", oradores);

        RequestDispatcher dispatcher = req.getRequestDispatcher("/views/OradoresAdmin.jsp");
        dispatcher.forward(req, res);
    }
}
