package models;

import utils.MappingUtils;
import java.sql.Date;
import java.sql.ResultSet;
import interfaces.MapperInterface;

public class OradorModel implements MapperInterface<OradorModel> {
    public Integer id;
    public String email;
    public String nombres;
    public String apellidos;
    public String tema;
    public Date fechaHoraCreacion;
    public Date fechaHoraModificacion;
    public Date fechaEvento;
    public Boolean aprobado;
    public UsuarioModel usuarioCreador;
    public UsuarioModel usuarioModificador;

    @Override
    public OradorModel map(ResultSet result) {
        this.id = MappingUtils.getValue(result, "id", Integer.class);
        this.email = MappingUtils.getValue(result, "email", String.class);
        this.nombres = MappingUtils.getValue(result, "nombres", String.class);
        this.apellidos = MappingUtils.getValue(result, "apellidos", String.class);
        this.tema = MappingUtils.getValue(result, "tema", String.class);
        this.fechaHoraCreacion = MappingUtils.getValue(result, "f_h_creacion", Date.class);
        this.fechaHoraModificacion = MappingUtils.getValue(result, "f_h_creacion", Date.class);
        this.fechaEvento = MappingUtils.getValue(result, "f_evento", Date.class);
        this.aprobado = MappingUtils.getValue(result, "aprobado", Boolean.class);

        this.usuarioCreador = this.getUsuarioCreador(
                MappingUtils.getValue(result, "id_usuario_creador", Integer.class));

        this.usuarioModificador = this.getUsuarioModificador(
                MappingUtils.getValue(result, "id_usuario_modificador", Integer.class));

        return this;
    }

    private UsuarioModel getUsuarioCreador(Integer id) {
        // TODO: implementar llamada a repository para traer el dato
        return new UsuarioModel();
    }

    private UsuarioModel getUsuarioModificador(Integer id) {
        // TODO: implementar llamada a repository para traer el dato
        return new UsuarioModel();
    }
}
