package models;

import java.sql.Date;
import java.sql.ResultSet;
import interfaces.MapperInterface;
import utils.MappingUtils;

public class UsuarioModel implements MapperInterface<UsuarioModel> {
    public Integer id;
    public String email;
    public String nombres;
    public String apellidos;
    public String password;
    public Date fechaHoraCreacion;
    public Date fechaHoraModificacion;
    public UsuarioModel usuarioCreador;
    public UsuarioModel usuarioModificador;
    public String rol;

    @Override
    public UsuarioModel map(ResultSet result) {
        try {
            this.id = MappingUtils.getValue(result, "id", Integer.class);
            this.email = MappingUtils.getValue(result, "email", String.class);
            this.nombres = MappingUtils.getValue(result, "nombres", String.class);
            this.apellidos = MappingUtils.getValue(result, "apellidos", String.class);
            this.password = MappingUtils.getValue(result, "password", String.class);
            this.fechaHoraCreacion = MappingUtils.getValue(result, "f_h_creacion", Date.class);
            this.fechaHoraModificacion = MappingUtils.getValue(result, "f_h_modificacion", Date.class);
            this.rol = MappingUtils.getValue(result, "rol", String.class);

            this.usuarioCreador = this.getUsuarioCreador(
                    MappingUtils.getValue(result, "id_usuario_creador", Integer.class));

            this.usuarioModificador = this.getUsuarioModificador(
                    MappingUtils.getValue(result, "id_usuario_modificador", Integer.class));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        return this;
    }

    private UsuarioModel getUsuarioCreador(Integer id) {
        // TODO: implementar llamada a repository para traer el dato
        return new UsuarioModel();
    }

    private UsuarioModel getUsuarioModificador(Integer id) {
        // TODO: implementar llamada a repository para traer el dato
        return new UsuarioModel();
    }
}
