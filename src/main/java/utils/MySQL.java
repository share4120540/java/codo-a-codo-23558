package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public abstract class MySQL {
    private static String connectionString = Environment.get("MYSQL_CONN_STRING") != null
            ? Environment.get("MYSQL_CONN_STRING")
            : "jdbc:mysql://tp-java-database:3306/tp_integrador_java?user=root&password=123456";

    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static Connection getConnection() throws ClassNotFoundException {
        try {
            Connection conn = DriverManager.getConnection(connectionString);
            return conn;
        } catch (SQLException e) {
            System.err.println(e.getMessage());
            System.err.println(e.getSQLState());
            System.err.println(e.getErrorCode());
            return null;
        }
    }
}
