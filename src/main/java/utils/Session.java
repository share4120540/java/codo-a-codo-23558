package utils;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.digest.DigestUtils;
import models.UsuarioModel;
import repositories.LoginRepository;
import javax.servlet.http.HttpSession;

public class Session {
    public static boolean isLogged(HttpServletRequest request) {
        HttpSession session = request.getSession();
        UsuarioModel usuario = (UsuarioModel) session.getAttribute("usuario");

        return usuario != null && usuario.id != 0;
    }

    public static boolean isAdmin(HttpServletRequest request) {
        HttpSession session = request.getSession();
        UsuarioModel usuario = (UsuarioModel) session.getAttribute("usuario");

        return usuario != null && usuario.rol.equals("ADMIN");
    }

    public static boolean validateLogin(HttpServletRequest request) {
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        LoginRepository loginRepo = new LoginRepository();
        UsuarioModel usuario = loginRepo.getUser(email, password);

        HttpSession session = request.getSession();

        if (usuario.id == null) {
            session.setAttribute("loginError", "No se pudo iniciar la sesión. Verifique usuario y contraseña");
            return false;
        }

        session.setAttribute("usuario", usuario);
        return true;
    }

    public static void cerrarSesion(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute("usuario");
    }
}
