package utils;

import java.sql.ResultSet;

public abstract class MappingUtils {
    // Ejemplo de uso: MappingUtils.getValue(result, "id", Integer.class)
    public static <T> T getValue(ResultSet result, String key, Class<T> type) {
        try {
            T value = type.cast(result.getObject(key));
            return value;
        } catch (Exception e) {
            return null;
        }
    }
}
