package utils;

import java.util.Map;

public abstract class Environment {
    public static String get(String key) {
        try {
            Map<String, String> env = System.getenv();
            String value = env.get(key);
            return value;
        } catch (Exception ex) {
            return null;
        }
    }
}
