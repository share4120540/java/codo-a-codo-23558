package interfaces;

import java.sql.ResultSet;

public interface MapperInterface<T> {
    public T map(ResultSet result);
}
