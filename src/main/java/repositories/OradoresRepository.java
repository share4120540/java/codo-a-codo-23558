package repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.management.OperationsException;
import models.OradorModel;
import utils.MySQL;

public class OradoresRepository {
    public ArrayList<OradorModel> getOradoresAprobadosSummary() {
        try {
            String query = "select nombres, apellidos, tema, f_evento from solicitud_oradores where aprobado = 1";
            Connection connection = MySQL.getConnection();
            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery(query);
            ArrayList<OradorModel> oradores = new ArrayList<OradorModel>();

            while (result.next()) {
                OradorModel orador = new OradorModel();
                oradores.add(orador.map(result));
            }

            statement.close();
            connection.close();
            return oradores;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            return new ArrayList<OradorModel>();
        }
    }

    public ArrayList<OradorModel> getOradores() {
        try {
            String query = "select id, nombres, apellidos, tema, f_evento, f_h_creacion, aprobado, id_usuario_creador, "
                    + "id_usuario_modificador from solicitud_oradores";
            Connection connection = MySQL.getConnection();
            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery(query);
            ArrayList<OradorModel> oradores = new ArrayList<OradorModel>();

            while (result.next()) {
                OradorModel orador = new OradorModel();
                oradores.add(orador.map(result));
            }

            statement.close();
            connection.close();
            return oradores;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            return new ArrayList<OradorModel>();
        }
    }

    public OradorModel getOrador(Integer id) {
        try {
            String query = "select id, nombres, apellidos, tema, f_evento, f_h_creacion, f_h_modificacion, "
                    + "aprobado, id_usuario_creador, id_usuario_modificador from solicitud_oradores where id = ?";
            Connection connection = MySQL.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);

            statement.setInt(1, id);

            ResultSet result = statement.executeQuery();
            OradorModel usuario = new OradorModel();

            if (result.next()) {
                usuario.map(result);
            }

            statement.close();
            connection.close();
            return usuario;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            return new OradorModel();
        }
    }

    public OradorModel saveOradorFromIndex(OradorModel orador) {
        try {
            String query = "insert into solicitud_oradores (email, nombres, apellidos, tema) values (?,?,?,?)";
            Connection connection = MySQL.getConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, orador.email);
            statement.setString(2, orador.nombres);
            statement.setString(3, orador.apellidos);
            statement.setString(4, orador.tema);

            Integer count = statement.executeUpdate();

            if (count == 0) {
                statement.close();
                connection.close();
                throw new OperationsException("No se pudo guardar el orador en bd");
            }

            ResultSet result = statement.getGeneratedKeys();
            OradorModel oradorInsertado = new OradorModel();

            if (result.next()) {
                oradorInsertado.map(result);
            }

            statement.close();
            connection.close();
            return oradorInsertado;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            return new OradorModel();
        }
    }

    public OradorModel insertOrador(OradorModel orador) {
        try {
            String query = "insert into solicitud_oradores (email, nombres, apellidos, tema, id_usuario_creador, "
                    + "aprobado, f_evento) values (?,?,?,?,?,?,?)";
            Connection connection = MySQL.getConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, orador.email);
            statement.setString(2, orador.nombres);
            statement.setString(3, orador.apellidos);
            statement.setString(4, orador.tema);
            statement.setInt(5, orador.usuarioCreador.id);
            statement.setBoolean(6, orador.aprobado);
            statement.setDate(7, orador.fechaEvento);

            Integer count = statement.executeUpdate();

            if (count == 0) {
                statement.close();
                connection.close();
                throw new OperationsException("No se pudo guardar el orador en bd");
            }

            ResultSet result = statement.getGeneratedKeys();
            OradorModel oradorInsertado = new OradorModel();

            if (result.next()) {
                oradorInsertado.map(result);
            }

            statement.close();
            connection.close();
            return oradorInsertado;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            return new OradorModel();
        }
    }

    public OradorModel updateOrador(OradorModel orador) {
        try {
            String query = "update solicitud_oradores set email = ?, nombres = ?, apellidos = ?, tema = ?, "
                    + "id_usuario_modificador = ?, f_h_modificacion = ?, aprobado = ?, f_evento = ? where id = ?";
            Connection connection = MySQL.getConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, orador.email);
            statement.setString(2, orador.nombres);
            statement.setString(3, orador.apellidos);
            statement.setString(4, orador.tema);
            statement.setInt(5, orador.usuarioModificador.id);
            statement.setString(5, "CURRENT_TIMESTAMP");
            statement.setBoolean(7, orador.aprobado);
            statement.setDate(8, orador.fechaEvento);
            statement.setInt(9, orador.id);

            Integer count = statement.executeUpdate();

            if (count == 0) {
                statement.close();
                connection.close();
                throw new OperationsException("No se pudo actualizar el orador en bd");
            }

            ResultSet result = statement.getGeneratedKeys();
            OradorModel oradorInsertado = new OradorModel();

            if (result.next()) {
                oradorInsertado.map(result);
            }

            statement.close();
            connection.close();
            return oradorInsertado;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            return new OradorModel();
        }
    }

    public Boolean deleteOrador(Integer id) {
        try {
            String query = "delete from solicitud_oradores where id = ?";
            Connection connection = MySQL.getConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.SUCCESS_NO_INFO);

            statement.setInt(1, id);

            Integer count = statement.executeUpdate();

            if (count == 0) {
                statement.close();
                connection.close();
                throw new OperationsException("No se pudo eliminar el orador en bd");
            }

            statement.close();
            connection.close();
            return true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            return false;
        }
    }
}
