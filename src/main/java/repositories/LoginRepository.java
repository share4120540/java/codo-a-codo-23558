package repositories;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.management.OperationsException;
import java.sql.PreparedStatement;
import org.apache.commons.codec.digest.DigestUtils;
import models.UsuarioModel;
import utils.MySQL;

public class LoginRepository {
    public UsuarioModel getUser(String email, String password) {
        try {
            String query = "select id, email, nombres, apellidos, rol from usuarios where email = ? and password = ?";
            Connection connection = MySQL.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);

            statement.setString(1, email);
            statement.setString(2, DigestUtils.sha256Hex(password));

            ResultSet result = statement.executeQuery();
            UsuarioModel usuario = new UsuarioModel();

            if (result.next()) {
                usuario.map(result);
            }

            statement.close();
            connection.close();
            return usuario;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            return new UsuarioModel();
        }
    }

    public UsuarioModel getUser(Integer id) {
        try {
            String query = "select id, email, nombres, apellidos, rol from usuarios where id = ?;";
            Connection connection = MySQL.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);

            statement.setInt(1, id);

            ResultSet result = statement.executeQuery();
            UsuarioModel usuario = new UsuarioModel();

            if (result.next()) {
                usuario.map(result);
            }

            statement.close();
            connection.close();
            return usuario;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            return new UsuarioModel();
        }
    }

    public UsuarioModel saveUser(UsuarioModel usuario) {
        try {
            String query = "insert into usuarios (email, nombres, apellidos, password) values (?,?,?,?)";
            Connection connection = MySQL.getConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, usuario.email);
            statement.setString(2, usuario.nombres);
            statement.setString(3, usuario.apellidos);
            statement.setString(4, usuario.password);

            Integer count = statement.executeUpdate();

            if (count == 0) {
                statement.close();
                connection.close();
                throw new OperationsException("No se pudo guardar el usuario en bd");
            }

            ResultSet result = statement.getGeneratedKeys();
            UsuarioModel usuarioInsertado = new UsuarioModel();

            if (result.next()) {
                usuarioInsertado.map(result);
            }

            statement.close();
            connection.close();
            return usuarioInsertado;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            return new UsuarioModel();
        }
    }
}
