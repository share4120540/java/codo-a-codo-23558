package repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.management.OperationsException;
import models.UsuarioModel;
import utils.MySQL;

public class UsuariosRepository {
    public ArrayList<UsuarioModel> getUsuarios() {
        try {
            String query = "select id, nombres, apellidos, email from usuarios";
            Connection connection = MySQL.getConnection();
            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery(query);
            ArrayList<UsuarioModel> oradores = new ArrayList<UsuarioModel>();

            while (result.next()) {
                UsuarioModel orador = new UsuarioModel();
                oradores.add(orador.map(result));
            }

            statement.close();
            connection.close();
            return oradores;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            return new ArrayList<UsuarioModel>();
        }
    }

    public UsuarioModel getUsuario(Integer id) {
        try {
            String query = "select id, nombres, apellidos, email, password, f_h_creacion, f_h_modificacion, "
                    + "id_usuario_creador, id_usuario_modificador from usuarios where id = ?";
            Connection connection = MySQL.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);

            statement.setInt(1, id);

            ResultSet result = statement.executeQuery();
            UsuarioModel usuario = new UsuarioModel();

            if (result.next()) {
                usuario.map(result);
            }

            statement.close();
            connection.close();
            return usuario;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            return new UsuarioModel();
        }
    }

    public UsuarioModel insertUsuario(UsuarioModel usuario) {
        try {
            String query = "insert into usuarios (email, nombres, apellidos, password, id_usuario_creador) values (?,?,?,?,?)";
            Connection connection = MySQL.getConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, usuario.email);
            statement.setString(2, usuario.nombres);
            statement.setString(3, usuario.apellidos);
            statement.setString(4, usuario.password);
            statement.setInt(5, usuario.usuarioCreador.id);

            Integer count = statement.executeUpdate();

            if (count == 0) {
                statement.close();
                connection.close();
                throw new OperationsException("No se pudo guardar el usuario en bd");
            }

            ResultSet result = statement.getGeneratedKeys();
            UsuarioModel oradorInsertado = new UsuarioModel();

            if (result.next()) {
                oradorInsertado.map(result);
            }

            statement.close();
            connection.close();
            return oradorInsertado;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            return new UsuarioModel();
        }
    }

    public UsuarioModel updateOrador(UsuarioModel orador) {
        try {
            String query = "update usuarios set email = ?, nombres = ?, apellidos = ?, password = ?, "
                    + "id_usuario_modificador = ?, f_h_modificacion = ? where id = ?";
            Connection connection = MySQL.getConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            statement.setString(1, orador.email);
            statement.setString(2, orador.nombres);
            statement.setString(3, orador.apellidos);
            statement.setString(4, orador.password);
            statement.setInt(5, orador.usuarioModificador.id);
            statement.setString(6, "CURRENT_TIMESTAMP");
            statement.setInt(7, orador.id);

            Integer count = statement.executeUpdate();

            if (count == 0) {
                statement.close();
                connection.close();
                throw new OperationsException("No se pudo actualizar el usuario en bd");
            }

            ResultSet result = statement.getGeneratedKeys();
            UsuarioModel oradorInsertado = new UsuarioModel();

            if (result.next()) {
                oradorInsertado.map(result);
            }

            statement.close();
            connection.close();
            return oradorInsertado;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            return new UsuarioModel();
        }
    }

    public Boolean deleteUsuario(Integer id) {
        try {
            String query = "delete from usuarios where id = ?";
            Connection connection = MySQL.getConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.SUCCESS_NO_INFO);

            statement.setInt(1, id);

            Integer count = statement.executeUpdate();

            if (count == 0) {
                statement.close();
                connection.close();
                throw new OperationsException("No se pudo eliminar el usuario en bd");
            }

            statement.close();
            connection.close();
            return true;
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println(e.getCause());
            System.err.println(e.getStackTrace());
            return false;
        }
    }
}
