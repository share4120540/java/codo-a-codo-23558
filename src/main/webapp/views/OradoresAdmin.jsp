<!DOCTYPE html>
<html lang="en">

    <head>
        <!--noformat-->
        <%@ page import="javax.servlet.http.HttpSession" %>
        <%@ page import="models.OradorModel" %>
        <%@ page import="java.util.ArrayList" %>
        <%@ page import="java.text.SimpleDateFormat" %>
        <%@ page contentType="text/html;charset=UTF-8" language="java" %>
        <!--noformat-->

        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet"
              href="../public/css/style.css">
        <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
              rel="stylesheet"
              integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN"
              crossorigin="anonymous"> -->
        <link rel="stylesheet"
              href="../public/css/bootstrap-lux.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
                crossorigin="anonymous"></script>

        <title>Oradores Admin</title>
    </head>

    <body>
        <header>
            <nav class="navbar navbar-expand-lg bg-body-tertiary"
                 data-bs-theme="dark">
                <div class="container-md">

                    <a class="navbar-brand"
                       href="dashboard">
                        <img src="../public/assets/images/codoacodo.png"
                             alt="Logo"
                             width="100"
                             class="d-inline-block align-text-middle">
                        Admin Dashboard
                    </a>

                    <button class="navbar-toggler"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#navbarNavAltMarkup"
                            aria-controls="navbarNavAltMarkup"
                            aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse"
                         id="navbarNavAltMarkup">
                        <div class="navbar-nav ms-auto">
                            <a class="nav-link active"
                               aria-current="page"
                               href="oradores">Oradores</a>
                            <a class="nav-link"
                               aria-current="page"
                               href="usuarios">Usuarios</a>
                        </div>
                        <div class="ms-lg-5">
                            <a class="btn btn-outline-secondary"
                               role="button"
                               href="logout">Logout</a>
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <section id="Info"
                 class="container-md">
            <div class="row">
                <div class="col dvh-65 pt-3 pb-2">
                    <div class="table-responsive">
                        <table class="table table-primary table-striped table-dark">
                            <thead>
                                <tr>
                                    <th scope="col">Orador</th>
                                    <th scope="col">Tema</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!--noformat-->
                                <% if(session.getAttribute("oradores") != null) {
                                    ArrayList<OradorModel> oradores = (ArrayList<OradorModel>)session.getAttribute("oradores");
                                    SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
        
                                    for(OradorModel orador: oradores) { 
                                        String fecha = orador.fechaEvento != null ? formater.format(orador.fechaEvento) : "-";  
                                    %>
                                        <tr class="">
                                            <td scope="row"><%= orador.nombres %> <%= orador.apellidos %></td>
                                            <td><%= orador.tema %></td>
                                            <td><%= fecha %></td>
                                            <td>
                                                <a class="btn btn-info" 
                                                   role="button" 
                                                   href="oradores?action=view&id=<%= orador.id %>">Ver</a>
                                                <a class="btn btn-primary" 
                                                   role="button" 
                                                   href="oradores?action=edit&id=<%= orador.id %>">Editar</a>
                                                <a class="btn btn-danger" 
                                                   role="button" 
                                                   href="oradores?action=delete&id=<%= orador.id %>">Eliminar</a>
                                            </td>
                                        </tr>
                                    <% } 
                                 } else { %>
                                <!--noformat-->
                                <tr class="">
                                    <td scope="row"
                                        colspan="4">
                                        No hay registros que mostrar
                                    </td>
                                </tr>
                                <!--noformat-->
                                <% } %>
                                <!--noformat-->
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td scope="row"
                                        colspan="4">
                                        <a href="oradores?action=create"
                                           class="btn btn-success"
                                           role="button">Nuevo</a>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </section>

        <footer class="bg-dark text-center mt-3 pt-5 pb-5 container-fluid text-light">
            <div class="container-md mt-3 gx-0 row justify-content-between ms-auto me-auto">
                <div class="col-3"></div>
                <div class="col-6">
                    <p>
                        Trabajo práctico integrador final en Java Servlets, desarrollado y mantenido por
                        <strong>Federico Schroh</strong>
                    </p>
                </div>
                <div class="col-3"></div>
            </div>
        </footer>

    </body>

</html>