<!DOCTYPE html>
<html lang="en">

    <head>
        <!--noformat-->
        <%@ page import="javax.servlet.http.HttpSession" %>
        <%@ page contentType="text/html;charset=UTF-8" language="java" %>
        <!--noformat-->

        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet"
              href="../public/css/style.css">
        <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN"
            crossorigin="anonymous"> -->
        <link rel="stylesheet"
              href="../public/css/bootstrap-lux.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
                crossorigin="anonymous"></script>
        <script src="../public/js/login-form-utils.js"></script>

        <title>Login</title>
    </head>

    <body>
        <div class="container-md mt-5">
            <div class="row justify-content-center">

                <div class="card mb-3"
                     style="max-width: 30rem;">
                    <div class="card-header bg-body-tertiary mt-3 text-light"
                         data-bs-theme="dark">Acceso al dashboard</div>
                    <div class="card-body">
                        <h4 class="card-title">Bienvenido!</h4>
                        <form action="login"
                              method="post"
                              class="form mt-3"
                              name="form"
                              id="form">
                            <div class="mb-3 col">
                                <label for="email"
                                       class="form-label">Email</label>
                                <input type="email"
                                       name="email"
                                       id="email"
                                       class="form-control"
                                       placeholder="Su email"
                                       required />

                            </div>

                            <div class="mb-3 col">
                                <label for="password"
                                       class="form-label">Contraseña</label>
                                <input type="password"
                                       name="password"
                                       id="password"
                                       class="form-control"
                                       placeholder="Su contraseña"
                                       required
                                       minlength="8" />

                            </div>

                            <!--noformat-->
                            <% if(session.getAttribute("loginError") != null) {
                               String loginError=(String)session.getAttribute("loginError");
                               session.removeAttribute("loginError");
                            %>
                                <div class="col mb-3">
                                    <div class="alert alert-danger"
                                         role="alert">
                                        <strong>Oops!</strong> <%= loginError %>
                                    </div>
                                </div>
                            <% } %>
                            <!--noformat-->

                            <div class="d-grid gap-2 mt-5">
                                <a class="card-text"
                                   href="#">Olvidé mi contraseña</a>
                                <a class="card-text"
                                   href="#">Solicitar acceso</a>
                            </div>

                            <div class="d-grid gap-2 mt-5">
                                <button type="submit"
                                        name="login"
                                        id="btn-login"
                                        class="btn btn-success"
                                        disabled="disabled">
                                    Iniciar Sesión
                                </button>
                                <a href="/"
                                   class="btn btn-secondary"
                                   role="button">Ir al Home</a>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

    </body>

</html>