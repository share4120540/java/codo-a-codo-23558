<!DOCTYPE html>
<html lang="en">

    <head>
        <!--noformat-->
        <%@ page contentType="text/html;charset=UTF-8" language="java" %>
        <!--noformat-->

        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet"
              href="../public/css/style.css">
        <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN"
            crossorigin="anonymous"> -->
        <link rel="stylesheet"
              href="../public/css/bootstrap-lux.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
                crossorigin="anonymous"></script>

        <script src="../public/js/utils.js"></script>
        <script src="../public/js/load-prices.js"></script>
        <script src="../public/js/tickets-form-utils.js"></script>

        <title>Conf. Bs. As.</title>
    </head>

    <body>
        <header>
            <nav class="navbar navbar-expand-lg bg-body-tertiary"
                 data-bs-theme="dark">
                <div class="container-md">

                    <a class="navbar-brand"
                       href="#">
                        <img src="../public/assets/images/codoacodo.png"
                             alt="Logo"
                             width="100"
                             class="d-inline-block align-text-middle">
                        Conf Bs As
                    </a>

                    <button class="navbar-toggler"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#navbarNavAltMarkup"
                            aria-controls="navbarNavAltMarkup"
                            aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse"
                         id="navbarNavAltMarkup">
                        <div class="navbar-nav ms-auto">
                            <a class="nav-link"
                               aria-current="page"
                               href="index#slider">La conferencia</a>
                            <a class="nav-link"
                               aria-current="page"
                               href="index#oradores">Los oradores</a>
                            <a class="nav-link"
                               href="index#info">El lugar y la fecha</a>
                            <a class="nav-link"
                               href="index#contacto">Conviértete en orador</a>
                            <a class="nav-link text-success active"
                               href="#">Comprar tickets</a>
                        </div>
                        <div class="ms-5">
                            <a class="btn btn-outline-secondary"
                               role="button"
                               href="login">Acceso</a>
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <section id="venta"
                 class="container-md">
            <div class="row pt-3">
                <div class="col-md-4 pb-3">
                    <div class="card text-center border-info">
                        <div class="card-body">
                            <h4 class="card-title">Estudiante</h4>
                            <p class="card-text">Tiene un descuento</p>
                            <p class="card-text">
                                <strong><span id="descuento-estudiante">0</span>%</strong>
                            </p>
                            <p class="card-text">
                                <small>* presentar documentación</small>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 pb-3">
                    <div class="card text-center border-success">
                        <div class="
                         card-body">
                            <h4 class="card-title">Trainee</h4>
                            <p class="card-text">Tiene un descuento</p>
                            <p class="card-text">
                                <strong>
                                    <span id="descuento-trainee">0</span>%
                                </strong>
                            </p>
                            <p class="card-text">
                                <small>* presentar documentación</small>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 pb-3">
                    <div class="card text-center border-warning">
                        <div class="card-body">
                            <h4 class="card-title">Junior</h4>
                            <p class="card-text">Tiene un descuento</p>
                            <p class="card-text">
                                <strong>
                                    <span id="descuento-junior">0</span>%
                                </strong>
                            </p>
                            <p class="card-text">
                                <small>* presentar documentación</small>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col text-center text-uppercase pt-3 pb-2">
                    <p class="subtitle">Venta</p>
                    <h3 class="title">
                        Valor de Ticket $<span id="valor-ticket">0</span>
                    </h3>
                </div>
            </div>

            <form class="row"
                  action="#"
                  id="form">
                <div class="col-md-6 mb-3">
                    <input type="text"
                           name="nombre"
                           id="nombre"
                           class="form-control"
                           placeholder="Nombre"
                           required>
                </div>

                <div class="col-md-6 mb-3">
                    <input type="text"
                           name="apellido"
                           id="apellido"
                           class="form-control"
                           placeholder="Apellido"
                           required>
                </div>

                <div class="col-12 mb-3">
                    <input type="email"
                           name="correo"
                           id="correo"
                           class="form-control"
                           placeholder="Correo"
                           required>
                </div>

                <div class="col-6 mb-3">
                    <div class="form-group">
                        <label for="cantidad">Cantidad</label>
                        <input type="number"
                               name="cantidad"
                               min="1"
                               id="cantidad"
                               class="form-control"
                               placeholder=""
                               required>
                    </div>
                </div>

                <div class="col-6 mb-3">
                    <div class="form-group">
                        <label for="categoria">Categoria</label>
                        <select id="categoria"
                                class="form-control"
                                required>
                            <option selected>Estudiante</option>
                            <option>Trainee</option>
                            <option>Junior</option>
                        </select>
                    </div>
                </div>

                <div class="col-12 mb-3">
                    <p class="alert alert-info p-3 text-info">
                        Total a Pagar: $<span id="total-pagar">0</span>
                    </p>
                </div>

                <div class="col-6">
                    <button type="reset"
                            id="btn-reset"
                            class="btn btn-success w-100">Borrar</button>
                </div>
                <div class="col-6">
                    <button type="button"
                            class="btn btn-success w-100"
                            id="btn-resumen"
                            data-bs-toggle="modal"
                            data-bs-target="#resumenModal"
                            disabled>Resumen</button>
                </div>
            </form>
        </section>

        <section id="resumen">
            <div class="modal fade"
                 id="resumenModal"
                 tabindex="-1"
                 role="dialog"
                 aria-labelledby="modalLabel"
                 aria-hidden="true">
                <div class="modal-dialog"
                     role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5"
                                id="modalLabel">Resumen</h1>
                            <button type="button"
                                    class="close"
                                    data-bs-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="resumen-nombre">Nombre</label>
                                    <div class="text-uppercase">
                                        <strong>
                                            <span id="resumen-nombre">Lorem ipsum</span>
                                        </strong>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="resumen-apellido">Apellido</label>
                                    <div class="text-uppercase">
                                        <strong>
                                            <span id="resumen-apellido">Lorem ipsum</span>
                                        </strong>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="resumen-correo">Correo</label>
                                    <div class="text-uppercase">
                                        <strong>
                                            <span id="resumen-correo">Lorem@ipsum.lorem</span>
                                        </strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-3 mb-3">
                                    <label for="resumen-cantidad">Cantidad</label>
                                    <div class="text-uppercase">
                                        <strong>
                                            <span id="resumen-cantidad">0</span>
                                        </strong>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="resumen-categoria">Categoría</label>
                                    <div class="text-uppercase">
                                        <strong>
                                            <span id="resumen-categoria">Lorem</span>
                                        </strong>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="resumen-unitario">Precio Unitario</label>
                                    <div class="text-uppercase">
                                        <strong>
                                            $<span id="resumen-unitario">0</span>
                                        </strong>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label for="resumen-descuento">Descuento</label>
                                    <div class="text-uppercase">
                                        <strong>
                                            <span id="resumen-descuento">0</span>%
                                        </strong>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-12">
                                    <label for="resumen-total">Total a Pagar</label>
                                    <div class="text-uppercase">
                                        <strong>
                                            $<span id="resumen-total">0</span>
                                        </strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button"
                                    class="btn btn-danger"
                                    data-bs-dismiss="modal">Cancelar</button>
                            <button type="button"
                                    class="btn btn-success">Comprar</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer class="bg-dark text-center mt-3 pt-5 pb-5 container-fluid text-light">
            <div class="container-md mt-3 gx-0 row justify-content-between ms-auto me-auto">
                <div class="col-md-1">
                    <a href="#"
                       class="nav-link">Preguntas frecuentes</a>
                </div>
                <div class="col-md-1">
                    <a href="#"
                       class="nav-link">Contáctanos</a>
                </div>
                <div class="col-md-1">
                    <a href="#"
                       class="nav-link">Prensa</a>
                </div>
                <div class="col-md-1">
                    <a href="#"
                       class="nav-link">Conferencias</a>
                </div>
                <div class="col-md-1">
                    <a href="#"
                       class="nav-link">Términos y condiciones</a>
                </div>
                <div class="col-md-1">
                    <a href="#"
                       class="nav-link">Privacidad</a>
                </div>
                <div class="col-md-1">
                    <a href="#"
                       class="nav-link">Estudiantes</a>
                </div>
            </div>
        </footer>

    </body>

</html>