<!DOCTYPE html>
<html lang="en">

    <head>
        <!--noformat-->
        <%@ page import="javax.servlet.http.HttpSession" %>
        <%@ page import="models.OradorModel" %>
        <%@ page import="java.util.ArrayList" %>
        <%@ page import="java.text.SimpleDateFormat" %>
        <%@ page contentType="text/html;charset=UTF-8" language="java" %>
        <!--noformat-->

        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet"
              href="../public/css/style.css">
        <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
              rel="stylesheet"
              integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN"
              crossorigin="anonymous"> -->
        <link rel="stylesheet"
              href="../public/css/bootstrap-lux.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL"
                crossorigin="anonymous"></script>
        <script src="../public/js/index-form-utils.js"></script>

        <title>Conf. Bs. As.</title>
    </head>

    <body>
        <header>
            <nav class="navbar navbar-expand-lg bg-body-tertiary"
                 data-bs-theme="dark">
                <div class="container-md">

                    <a class="navbar-brand"
                       href="#">
                        <img src="../public/assets/images/codoacodo.png"
                             alt="Logo"
                             width="100"
                             class="d-inline-block align-text-middle">
                        Conf Bs As
                    </a>

                    <button class="navbar-toggler"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#navbarNavAltMarkup"
                            aria-controls="navbarNavAltMarkup"
                            aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse"
                         id="navbarNavAltMarkup">
                        <div class="navbar-nav ms-auto">
                            <a class="nav-link active"
                               aria-current="page"
                               href="#slider">La conferencia</a>
                            <a class="nav-link"
                               aria-current="page"
                               href="#oradores">Los oradores</a>
                            <a class="nav-link"
                               href="#info">El lugar y la fecha</a>
                            <a class="nav-link"
                               href="#contacto">Conviértete en orador</a>
                            <a class="nav-link text-success"
                               href="tickets">Comprar tickets</a>
                        </div>
                        <div class="ms-lg-5">
                            <a class="btn btn-outline-secondary"
                               role="button"
                               href="login">Acceso</a>
                        </div>
                    </div>
                </div>
            </nav>
        </header>
        <section id="slider"
                 class="carousel slide"
                 data-bs-ride="carousel">

            <div class="backdrop position-absolute w-100 h-100 opacity-50">
            </div>

            <div class="container-md center-container position-absolute focus-text text-end text-light d-flex h-100">
                <div class="row justify-content-end align-items-center">
                    <div class="col-md-6">
                        <h3 class="title text-light">Conf Bs As</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatibus
                            consectetur quod ex
                            accusantium suscipit qui doloribus sint nemo quasi voluptatum maiores quas facere id
                            ipsa maxime natus.</p>

                        <a href="#contacto"
                           class="btn btn-outline-secondary">Quiero ser orador</a>
                        <a href="tickets"
                           class="btn btn-success">Comprar</a>
                    </div>
                </div>
            </div>

            <div class="carousel-inner"
                 role="listbox">
                <div class="carousel-item active img-container">
                    <img src="../public/assets/images/hawaii.jpg"
                         class="d-block"
                         alt="First slide">
                </div>
                <div class="carousel-item img-container">
                    <img src="../public/assets/images/hawaii2.jpg"
                         class="d-block"
                         alt="Second slide">
                </div>
                <div class="carousel-item img-container">
                    <img src="../public/assets/images/hawaii3.jpg"
                         class="d-block"
                         alt="Third slide">
                </div>
            </div>
        </section>

        <section id="oradores"
                 class="container-md">
            <div class="row">
                <div class="col text-center text-uppercase pt-3 pb-2">
                    <p class="subtitle">Conoce a los</p>
                    <h3 class="title">Oradores</h3>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="card mt-2">
                        <img class="card-img-top"
                             src="../public/assets/images/steve.jpg"
                             alt="Title">
                        <div class="card-body">
                            <div class="pb-2">
                                <span class="badge bg-warning">Javascript</span>
                                <span class="badge bg-primary">React</span>
                            </div>
                            <h4 class="card-title">Steve Jobs</h4>
                            <p class="card-text">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab neque
                                non ipsam doloremque illo, adipisci nihil tempora corrupti laudantium perspiciatis
                                delectus libero ad, velit architecto. Dolorem porro labore ipsum voluptate iste,
                                laudantium commodi iusto repellat ipsam dolor non expedita possimus aliquid tempora
                                itaque corporis dolore quasi ut fugiat quibusdam hic.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card mt-2">
                        <img class="card-img-top"
                             src="../public/assets/images/bill.jpg"
                             alt="Title">
                        <div class="card-body">
                            <div class="pb-2">
                                <span class="badge bg-warning">Javascript</span>
                                <span class="badge bg-primary">React</span>
                            </div>
                            <h4 class="card-title">Bil Gates</h4>
                            <p class="card-text">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab neque
                                non ipsam doloremque illo, adipisci nihil tempora corrupti laudantium perspiciatis
                                delectus libero ad, velit architecto. Dolorem porro labore ipsum voluptate iste,
                                laudantium commodi iusto repellat ipsam dolor non expedita possimus aliquid tempora
                                itaque corporis dolore quasi ut fugiat quibusdam hic.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card mt-2">
                        <img class="card-img-top"
                             src="../public/assets/images/ada.jpeg"
                             alt="Title">
                        <div class="card-body">
                            <div class="pb-2">
                                <span class="badge bg-light">Negocio</span>
                                <span class="badge bg-danger">Stratup</span>
                            </div>
                            <h4 class="card-title">Ada Lovelace</h4>
                            <p class="card-text">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab neque
                                non ipsam doloremque illo, adipisci nihil tempora corrupti laudantium perspiciatis
                                delectus libero ad, velit architecto. Dolorem porro labore ipsum voluptate iste,
                                laudantium commodi iusto repellat ipsam dolor non expedita possimus aliquid tempora
                                itaque corporis dolore quasi ut fugiat quibusdam hic.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row"
                 id="oradores-aprobados">
                <div class="col text-center text-uppercase pt-3 pb-2">
                    <p class="subtitle">Otros oradores confirmados</p>
                    <!--noformat-->
                    <% if(session.getAttribute("oradoresAprobados") == null) { %>
                        <a href="solicitud-orador?aprobados=true"
                           class="btn btn-primary"
                           role="button">Mostrar listado</a>
                    <% } else { %>
                        <a href="solicitud-orador?aprobados=false"
                           class="btn btn-primary"
                           role="button">Ocultar Listado</a>
                    <% } %>
                    <!--noformat-->
                </div>
            </div>

            <!--noformat-->
            <% if(session.getAttribute("oradoresAprobados") != null) { %>
            <!--noformat-->
            <div class="row">
                <div class="table-responsive">
                    <table class="table table-primary table-striped table-dark">
                        <thead>
                            <tr>
                                <th scope="col">Orador</th>
                                <th scope="col">Tema</th>
                                <th scope="col">Fecha</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!--noformat-->
                            <% ArrayList<OradorModel> oradores = (ArrayList<OradorModel>)session.getAttribute("oradoresAprobados");
                            SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

                            for(OradorModel orador: oradores) { 
                                String fecha = formater.format(orador.fechaEvento);  
                            %>
                                <tr class="">
                                    <td scope="row"><%= orador.nombres %> <%= orador.apellidos %></td>
                                    <td><%= orador.tema %></td>
                                    <td><%= fecha %></td>
                                </tr>
                            <% } %>
                            <!--noformat-->
                        </tbody>
                    </table>
                </div>

            </div>
            <!--noformat-->
            <% } %>
            <!--noformat-->
        </section>

        <section id="info"
                 class="container-fluid mt-md-4 mt-2">
            <div class="row">
                <div class="col-md-6 gx-0">
                    <img src="../public/assets/images/honolulu.jpg"
                         alt="honolulu"
                         class="w-100">
                </div>

                <div class="col-md-6 gx-0 text-light">
                    <div class="bg-dark p-3 h-100">
                        <h3 class="title text-light">Bs As - Octubre</h3>
                        <p class="text">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. At, quia esse provident vero enim
                            consequuntur alias corporis atque minima necessitatibus commodi id expedita sequi. Quis
                            dicta
                            voluptatum quo ad impedit repellendus aut dolore eius aliquam nam nostrum similique in
                            soluta
                            omnis minima deserunt corporis nemo, inventore nobis accusamus eligendi dolorem. Quos eum
                            eos
                            provident aperiam minus dolore possimus, impedit unde, quis quibusdam eveniet totam, vel
                            numquam
                            amet voluptatem distinctio necessitatibus temporibus minima nihil harum ipsum molestias
                            aspernatur dolorem sunt? Officiis nobis, eaque amet reprehenderit optio cum cupiditate
                            explicabo, fuga tenetur quaerat distinctio quam dolore vero, quibusdam similique voluptates
                            sequi voluptate?
                        </p>
                        <p class="text">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. At, quia esse provident vero enim
                            consequuntur alias corporis atque minima necessitatibus commodi id expedita sequi. Quis
                            dicta
                            voluptatum quo ad impedit repellendus aut dolore eius aliquam nam nostrum similique in
                            soluta
                            omnis minima deserunt corporis nemo, inventore nobis accusamus eligendi dolorem. Quos eum
                        </p>
                        <a href="#"
                           class="btn btn-secondary">Conoce más</a>
                    </div>
                </div>
            </div>
        </section>

        <section id="contacto"
                 class="container-md">
            <div class="row">
                <div class="col text-center text-uppercase pt-3 pb-2">
                    <p class="subtitle">Conviértete en un</p>
                    <h3 class="title">Orador</h3>
                </div>
            </div>

            <div class="row">
                <div class="col text-center pb-3">
                    <span>
                        Anótate como orador para dar una
                        <a href="#"
                           target="_blank"
                           rel="noopener noreferrer">charla ignite.
                        </a>
                        Cuéntanos de qué quieres hablar
                    </span>.
                </div>
            </div>

            <form class="row"
                  action="solicitud-orador"
                  method="post"
                  name="form"
                  id="form">
                <div class="col-md-12 mb-3">
                    <input type="email"
                           name="email"
                           id="email"
                           class="form-control"
                           placeholder="Email"
                           required>
                </div>

                <div class="col-md-6 mb-3">
                    <input type="text"
                           name="nombres"
                           id="nombres"
                           class="form-control"
                           placeholder="Nombres"
                           required>
                </div>

                <div class="col-md-6 mb-3">
                    <input type="text"
                           name="apellidos"
                           id="apellidos"
                           class="form-control"
                           placeholder="Apellidos"
                           required>
                </div>

                <div class="col-12 mb-3">
                    <textarea class="form-control"
                              name="tema"
                              id="tema"
                              rows="3"
                              required>Sobre qué quieres hablar?
                    </textarea>
                </div>

                <div class="col-12 mb-3">
                    <!--noformat-->
                    <% if(session.getAttribute("solicitudOradorError") != null) {
                       String solicitudOradorError=(String)session.getAttribute("solicitudOradorError"); 
                       session.removeAttribute("solicitudOradorError");
                    %>
                        <div class="alert alert-danger"
                             role="alert">
                            <strong>Oops!</strong> <%= solicitudOradorError %>
                        </div>
                    <% } else if(session.getAttribute("solicitudOradorSuccess") != null) {
                           String solicitudOradorSuccess=(String)session.getAttribute("solicitudOradorSuccess");
                           session.removeAttribute("solicitudOradorSuccess");
                    %>
                        <div class="alert alert-success"
                             role="alert">
                            <strong>Genial!</strong> <%= solicitudOradorSuccess %>
                        </div>
                    <% } %>
                    <!--noformat-->
                </div>

                <div class="col-12">
                    <button type="submit"
                            name="btn-enviar-solicitud"
                            id="btn-enviar-solicitud"
                            class="btn btn-success w-100"
                            disabled="disabled">Enviar</button>
                </div>
            </form>
        </section>

        <footer class="bg-dark text-center mt-3 pt-5 pb-5 container-fluid text-light">
            <div class="container-md mt-3 gx-0 row justify-content-between ms-auto me-auto">
                <div class="col-md-1">
                    <a href="#"
                       class="nav-link">Preguntas frecuentes</a>
                </div>
                <div class="col-md-1">
                    <a href="#"
                       class="nav-link">Contáctanos</a>
                </div>
                <div class="col-md-1">
                    <a href="#"
                       class="nav-link">Prensa</a>
                </div>
                <div class="col-md-1">
                    <a href="#"
                       class="nav-link">Conferencias</a>
                </div>
                <div class="col-md-1">
                    <a href="#"
                       class="nav-link">Términos y condiciones</a>
                </div>
                <div class="col-md-1">
                    <a href="#"
                       class="nav-link">Privacidad</a>
                </div>
                <div class="col-md-1">
                    <a href="#"
                       class="nav-link">Estudiantes</a>
                </div>
            </div>
        </footer>

    </body>

</html>