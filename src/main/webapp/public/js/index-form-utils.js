let form;
let btnEnviar;
let nombres;
let apellidos;
let email;
let tema;

const getElementsAndSetListeners = () => {
    form = document.getElementById("form");
    btnEnviar = document.getElementById("btn-enviar-solicitud");
    nombres = document.getElementById("nombres");
    apellidos = document.getElementById("apellidos");
    email = document.getElementById("email");
    tema = document.getElementById("tema");

    setListenersValidarForm();
    validarForm(); //validar por si ya viene con datos cargados
}

const setListenersValidarForm = () => {
    nombres.addEventListener("change", validarForm, false);
    apellidos.addEventListener("change", validarForm, false);
    email.addEventListener("change", validarForm, false);
    tema.addEventListener("change", validarForm, false);
}

const validarForm = (event) => {
    if (form.checkValidity()) {
        btnEnviar.removeAttribute("disabled");
    } else {
        btnEnviar.setAttribute("disabled", "disabled");
    }
}

document.addEventListener("DOMContentLoaded", getElementsAndSetListeners, false);