let form;
let btnLogin;
let email;
let password;

const getElementsAndSetListeners = () => {
    form = document.getElementById("form");
    btnLogin = document.getElementById("btn-login");
    email = document.getElementById("email");
    password = document.getElementById("password");

    setListenersValidarForm();
    validarForm(); //validar por si ya viene con datos cargados
}

const setListenersValidarForm = () => {
    email.addEventListener("change", validarForm, false);
    password.addEventListener("change", validarForm, false);
}

const validarForm = (event) => {
    if (form.checkValidity()) {
        btnLogin.removeAttribute("disabled");
    } else {
        btnLogin.setAttribute("disabled", "disabled");
    }
}

document.addEventListener("DOMContentLoaded", getElementsAndSetListeners, false);