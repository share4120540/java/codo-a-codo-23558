let form;

let btnResumen;
let btnReset;

let nombre;
let apellido;
let correo;
let cantidad;
let categoria;
let totalPagar;

let resumenNombre;
let resumenApellido;
let resumenCorreo;
let resumenCantidad;
let resumenCategoria;
let resumenUnitario;
let resumenDescuento;
let resumenTotal;

let precioUnitario;

const getElementsAndSetListeners = () => {
    form = document.getElementById("form");

    btnResumen = document.getElementById("btn-resumen");
    btnReset = document.getElementById("btn-reset");

    nombre = document.getElementById("nombre");
    apellido = document.getElementById("apellido");
    correo = document.getElementById("correo");
    cantidad = document.getElementById("cantidad");
    categoria = document.getElementById("categoria");
    totalPagar = document.getElementById("total-pagar");

    resumenNombre = document.getElementById("resumen-nombre");
    resumenApellido = document.getElementById("resumen-apellido");
    resumenCorreo = document.getElementById("resumen-correo");
    resumenCantidad = document.getElementById("resumen-cantidad");
    resumenCategoria = document.getElementById("resumen-categoria");
    resumenUnitario = document.getElementById("resumen-unitario");
    resumenDescuento = document.getElementById("resumen-descuento");
    resumenTotal = document.getElementById("resumen-total");

    setListenersBotones();
    setListenersReplaceTotalPagar();
    setListenersValidarForm();
    validarForm(); //validar por si ya viene con datos cargados
}

const setListenersBotones = () => {
    btnResumen.addEventListener('click', cargarDatosResumen, false);
    btnReset.addEventListener('click', limpiarForm, false);
}

const setListenersReplaceTotalPagar = () => {
    cantidad.addEventListener("change", actualizarTotalPagar, false);
    categoria.addEventListener("change", actualizarTotalPagar, false);
}

const setListenersValidarForm = () => {
    nombre.addEventListener("change", validarForm, false);
    apellido.addEventListener("change", validarForm, false);
    correo.addEventListener("change", validarForm, false);
    cantidad.addEventListener("change", validarForm, false);
    categoria.addEventListener("change", validarForm, false);
}

const cargarDatosResumen = () => {
    resumenNombre.innerHTML = nombre.value;
    resumenApellido.innerHTML = apellido.value;
    resumenCorreo.innerHTML = correo.value;
    resumenCantidad.innerHTML = cantidad.value;
    resumenCategoria.innerHTML = categoria.value;
    resumenUnitario.innerHTML = precioUnitario;
    resumenDescuento.innerHTML = precios[`descuento${categoria.value}`] * 100;
    resumenTotal.innerHTML = totalPagar.innerHTML;
}

const actualizarTotalPagar = () => {
    const totalPagar = calcularTotalPagar();
    replaceText("total-pagar", totalPagar);
}

const validarForm = (event) => {
    if (form.checkValidity()) {
        btnResumen.removeAttribute("disabled");
    } else {
        btnResumen.setAttribute("disabled", "disabled");
    }
}

const limpiarForm = () => {
    //demás controles se limpian solos
    totalPagar.innerHTML = "0";
}

const calcularTotalPagar = () => {
    const cantidadSeleccionada = parseInt(cantidad.value);

    if (isNaN(cantidadSeleccionada)) {
        return 0;
    }

    const categoriaSeleccionada = categoria.value;
    const valorSinDescuento = precios.valorTicket * cantidadSeleccionada;
    const descuento = 1 - precios[`descuento${categoriaSeleccionada}`];

    //este precio unitario se usa en el resumen
    precioUnitario = Math.round(precios.valorTicket * descuento);

    return Math.round(valorSinDescuento * descuento);
}

document.addEventListener("DOMContentLoaded", getElementsAndSetListeners, false);