-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: localhost    Database: tp_integrador_java
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */
;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */
;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */
;
/*!50503 SET NAMES utf8mb4 */
;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */
;
/*!40103 SET TIME_ZONE='+00:00' */
;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */
;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */
;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */
;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */
;

--
-- Table structure for table `solicitud_oradores`
--

DROP TABLE IF EXISTS `solicitud_oradores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */
;
/*!50503 SET character_set_client = utf8mb4 */
;

CREATE TABLE
    `solicitud_oradores` (
        `id` int NOT NULL AUTO_INCREMENT,
        `email` varchar(100) NOT NULL,
        `nombres` varchar(100) NOT NULL,
        `apellidos` varchar(100) NOT NULL,
        `tema` varchar(1000) NOT NULL,
        `f_h_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `f_h_modificacion` datetime DEFAULT NULL,
        `f_evento` date DEFAULT NULL,
        `aprobado` tinyint(1) DEFAULT NULL,
        `id_usuario_creador` int DEFAULT NULL,
        `id_usuario_modificador` int DEFAULT NULL,
        PRIMARY KEY (`id`),
        KEY `id_usuario_creador` (`id_usuario_creador`),
        KEY `id_usuario_modificador` (`id_usuario_modificador`),
        CONSTRAINT `solicitud_oradores_ibfk_1` FOREIGN KEY (`id_usuario_creador`) REFERENCES `usuarios` (`id`),
        CONSTRAINT `solicitud_oradores_ibfk_2` FOREIGN KEY (`id_usuario_modificador`) REFERENCES `usuarios` (`id`)
    ) ENGINE = InnoDB AUTO_INCREMENT = 7 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */
;

--
-- Dumping data for table `solicitud_oradores`
--

LOCK TABLES `solicitud_oradores` WRITE;
/*!40000 ALTER TABLE `solicitud_oradores` DISABLE KEYS */
;

INSERT INTO
    `solicitud_oradores` (
        `id`,
        `email`,
        `nombres`,
        `apellidos`,
        `tema`,
        `f_evento`,
        `aprobado`
    )
VALUES (
        1,
        'email1@mail.com',
        'nombre1',
        'apellido1',
        'tema 1',
        '2023-12-20 02:19:38',
        NULL
    ), (
        2,
        'email2@mail.com',
        'nombre2',
        'apellido2',
        'tema 2',
        '2023-12-20 02:19:38',
        1
    ), (
        3,
        'email3@mail.com',
        'nombre3',
        'apellido3',
        'tema 3',
        '2023-12-20 02:19:38',
        1
    ), (
        4,
        'email4@mail.com',
        'nombre4',
        'apellido4',
        'tema 4',
        '2023-12-20 02:19:38',
        NULL
    ), (
        5,
        'email5@mail.com',
        'nombre5',
        'apellido5',
        'tema 5',
        '2023-12-20 02:19:38',
        NULL
    ), (
        6,
        'email6@mail.com',
        'nombre6',
        'apellido6',
        'tema 6',
        '2023-12-20 02:19:38',
        NULL
    );
/*!40000 ALTER TABLE `solicitud_oradores` ENABLE KEYS */
;

UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */
;
/*!50503 SET character_set_client = utf8mb4 */
;

CREATE TABLE
    `usuarios` (
        `id` int NOT NULL AUTO_INCREMENT,
        `email` varchar(100) NOT NULL,
        `password` varchar(100) NOT NULL,
        `nombres` varchar(100) NOT NULL,
        `apellidos` varchar(100) NOT NULL,
        `f_h_creacion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `f_h_modificacion` datetime DEFAULT NULL,
        `id_usuario_creador` int DEFAULT NULL,
        `id_usuario_modificador` int DEFAULT NULL,
        `rol` enum('ADMIN', 'USER') NOT NULL DEFAULT 'USER',
        PRIMARY KEY (`id`),
        KEY `id_usuario_creador` (`id_usuario_creador`),
        KEY `id_usuario_modificador` (`id_usuario_modificador`),
        CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_usuario_creador`) REFERENCES `usuarios` (`id`),
        CONSTRAINT `usuarios_ibfk_2` FOREIGN KEY (`id_usuario_modificador`) REFERENCES `usuarios` (`id`)
    ) ENGINE = InnoDB AUTO_INCREMENT = 2 DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */
;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */
;

/* password default es 12345678 */
INSERT INTO
    `usuarios` (
        `id`,
        `email`,
        `password`,
        `nombres`,
        `apellidos`,
        `rol`
    )
VALUES (
        1,
        'admin@mail.com',
        'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f',
        'admin',
        'default',
        'ADMIN'
    ), (
        2,
        'user@mail.com',
        'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f',
        'user',
        'default',
        'USER'
    );
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */
;

UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */
;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */
;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */
;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */
;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */
;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */
;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */
;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */
;

-- Dump completed on 2023-12-20  2:22:33