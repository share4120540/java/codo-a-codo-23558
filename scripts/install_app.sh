#!/bin/bash

echo "Comienza instalación de aplicación web"

echo "Removiendo instalación previa de aplicación web"
docker stop tp-java-app
docker rm tp-java-app

echo "Recompilando el proyecto java"
# mvn con flag -X muesta mas detalles de logs
cd ..
mvn clean install

echo "Generando nueva imagen docker de aplicación web"
docker build -t tp-java-app ./

echo "Iniciando el nuevo contenedor de aplicacion web 'tp-java-app'"
docker run -d --name=tp-java-app -p8080:8080 -p8000:8000 --network=tp-java-network tp-java-app

echo "Finaliza instalación de aplicacion web"