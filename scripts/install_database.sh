#!/bin/bash
echo "Comienza instalación de base de datos"

echo "Removimiendo instalación previa de base de datos"
docker stop tp-java-database
docker rm tp-java-database

echo "Iniciando el nuevo contenedor de base de datos 'tp-java-database'"
docker run -d -it --network tp-java-network -e MYSQL_ROOT_PASSWORD=123456 -e MYSQL_DATABASE=tp_integrador_java --name tp-java-database mysql:8.0.26

echo "Esperando 15 segundos para que termine de iniciar contenedor de base de datos..."
sleep 15

echo "Aplicando restore de base de datos inicial"
docker exec -i tp-java-database mysql -u root -p123456 tp_integrador_java < ./create_initial_schema.sql

echo "Finaliza instalación de base de datos"
